var app = angular.module('EmployeeApplication', []);

app.controller('EmployeeController',function($scope,$http) {
	
	$scope.showEmployees = true;
	
	$scope.employee = null;
	
    $scope.getEmployees = function() {
  	  $http({
		method: 'GET',
		url: '/employee-manager/rest/employee',
		headers: {'Content-Type': 'application/json'}
		}).success(function (result) {
    		$scope.employees = result;
    		$scope.showEmployees = true;
		});
    };

    $scope.deleteEmployee = function(employee) {
        $http({
			method:'DELETE',
			url: '/employee-manager/rest/employee/'+employee.id
	    }).success(function(data)
	    {
			$scope.getEmployees();
	    });
    };
    
    $scope.updateEmployee = function(employee) {
    	$scope.showEmployees = false;
    	$scope.employee = employee;
    }
    
    $scope.insertEmployee = function() {
    	$scope.employee = null;
    	$scope.showEmployees = false;
    }
    
    $scope.save = function(){
    	$http({
    		method: 'POST',
    		url: '/employee-manager/rest/employee',
    		headers: {'Content-Type': 'application/json'},
    		data: $scope.employee
    		}).success(function (result) {
    			$scope.getEmployees();
    	    	$scope.showEmployees = true;
    		});
    }
    
    $scope.modify = function(){
    	$http({
    		method: 'PUT',
    		url: '/employee-manager/rest/employee',
    		headers: {'Content-Type': 'application/json'},
    		data: $scope.employee
    		}).success(function (result) {
    			$scope.getEmployees();
    	    	$scope.showEmployees = true;
    		});
    }
    
    $scope.back = function(){
    	$scope.showEmployees = true;
    }
    
    $scope.getEmployees();
  });
