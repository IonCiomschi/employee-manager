package pentastagiu.employee_manager.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.employee_manager.model.Employee;
import pentastagiu.employee_manager.service.EmployeeService;

@Path("/employee")

public class EmployeeWS {
	@Inject
	private EmployeeService employeeService;

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee
	 * </pre>
	 * 
	 * Get all employees form database.
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getAllEmployee() {
		return employeeService.findAll();
	}

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee/employee.id
	 * </pre>
	 * 
	 * Get an employee form database by id.
	 * 
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployeeById(@PathParam("id") Long id) {
		return employeeService.findById(id);
	}

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee/name/employee.name
	 * </pre>
	 * 
	 * Get all employees from database with that name.
	 * 
	 */
	@GET
	@Path("name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployeeByName(@PathParam("name") String name) {
		return employeeService.findByName(name);
	}

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee/employee.id
	 * </pre>
	 * 
	 * Delete an employee form database by id.
	 * 
	 */
	@DELETE
	@Path("{id}")
	public Long deleteEmployeeById(@PathParam("id") Long id) {
		return employeeService.deleteEmployee(id);

	}

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee/
	 * </pre>
	 * 
	 * Insert an employee in database given in JSON format.
	 * 
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Employee insertEmployee(Employee employee) {
		return employeeService.insertEmployee(employee);
	}

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/employee/
	 * </pre>
	 * 
	 * Update an employee in database given in JSON format.
	 * 
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Employee updateEmployee(Employee employee) {
		return employeeService.updateEmployee(employee);
	}
}
