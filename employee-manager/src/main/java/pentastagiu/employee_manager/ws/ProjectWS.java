package pentastagiu.employee_manager.ws;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pentastagiu.employee_manager.model.Project;
import pentastagiu.employee_manager.service.ProjectService;

@Path("/project")
public class ProjectWS {
	@Inject
	private ProjectService projectService;

	/**
	 * <pre>
	 * URL: <host>[:port]/employee-manager/rest/project
	 * </pre>
	 * 
	 * Get all projects form database.
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Project> getAllProjects() {
		return projectService.findAllProjects();
	}

	/**
	 * <pre>
	 * URL: <host>[:port]employee-manager/rest/project/project.name
	 * </pre>
	 * 
	 * Get all employees from database with that name.
	 * 
	 */
	@GET
	@Path("{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Project getProjectByName(@PathParam("name") String name) {
		return projectService.findProjectByName(name);
	}
}
