package pentastagiu.employee_manager.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.employee_manager.dao.ProjectDAO;
import pentastagiu.employee_manager.model.Project;
import pentastagiu.employee_manager.service.ProjectService;

public class ProjectServiceImpl implements ProjectService {
	@Inject
	private ProjectDAO projectDAO;

	/**
	 * {@inheritDoc}
	 */
	public List<Project> findAllProjects() {
		return projectDAO.findAllProjects();
	}

	/**
	 * {@inheritDoc}
	 */
	public Project findProjectByName(String projectName) {
		return projectDAO.findProjectByName(projectName);
	}
}
