package pentastagiu.employee_manager.service.impl;

import java.util.List;

import javax.inject.Inject;

import pentastagiu.employee_manager.dao.EmployeeDAO;
import pentastagiu.employee_manager.model.Employee;
import pentastagiu.employee_manager.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {
	@Inject
	private EmployeeDAO employeeDAO;

	/**
	 * {@inheritDoc}
	 */
	public Employee insertEmployee(Employee employee) {
		return employeeDAO.insertEmployee(employee);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Employee> findByName(String employeeName) {
		return employeeDAO.findByName(employeeName);
	}

	/**
	 * {@inheritDoc}
	 */
	public Employee findById(Long employeeId) {
		return employeeDAO.findById(employeeId);
	}

	/**
	 * {@inheritDoc}
	 */
	public Long deleteEmployee(Long employeeId) {
		return employeeDAO.deleteEmployee(employeeId);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Employee> findAll() {
		return employeeDAO.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Employee updateEmployee(Employee employee) {
		return employeeDAO.updateEmployee(employee);
	}
}
