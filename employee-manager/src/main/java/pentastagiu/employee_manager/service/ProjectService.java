package pentastagiu.employee_manager.service;

import java.util.List;

import pentastagiu.employee_manager.model.Project;

public interface ProjectService {
	/**
	 * Lists all the projects in database.
	 */
	List<Project> findAllProjects();

	/**
	 * Gets project in database with a specific name.
	 */
	Project findProjectByName(String projectName);
}
