package pentastagiu.employee_manager.service;

import java.util.List;

import pentastagiu.employee_manager.model.Employee;

public interface EmployeeService {

	/**
	 * Inserts a new employee in the database
	 */
	Employee insertEmployee(Employee employee);

	/**
	 * Searches for a name
	 */
	List<Employee> findByName(String employeeName);

	/**
	 * Searches for an id in the Employee class
	 */
	Employee findById(Long employeeId);

	/**
	 * Deletes an employee specified by his ID
	 */
	Long deleteEmployee(Long employeeId);

	/**
	 * Lists all the employees present in the database
	 */
	List<Employee> findAll();

	/**
	 * Updates the employee specified in the parameter
	 */
	Employee updateEmployee(Employee employee);
}
