package pentastagiu.employee_manager.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pentastagiu.employee_manager.model.Project;

/**
 * Data Acces Object is used to store and obtain project data from the database.
 */
public class ProjectDAO {

	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("employee_persistence");
	private EntityManager entityManager = emfactory.createEntityManager();

	/**
	 * Returns a project with a specific name given as parameter.
	 */
	public Project findProjectByName(String projectName) {
		Query query = entityManager.createQuery("SELECT d FROM Project d WHERE d.projectName LIKE :name");
		query.setParameter("name", projectName);
		Project project = null;
		try {
			entityManager.getCriteriaBuilder();
			project = (Project) query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			project = null;
		}

		return project;
	}

	/**
	 * Gets all the projects in the database.
	 */
	public List<Project> findAllProjects() {
		Query query = entityManager.createQuery("SELECT p FROM Project p");
		List<Project> projectsList = new ArrayList<Project>();
		try {
			entityManager.getCriteriaBuilder();
			projectsList = query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return projectsList;
	}
}
