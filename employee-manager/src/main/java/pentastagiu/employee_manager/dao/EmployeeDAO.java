package pentastagiu.employee_manager.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pentastagiu.employee_manager.model.Department;
import pentastagiu.employee_manager.model.Employee;
import pentastagiu.employee_manager.model.Project;

/**
 * Data Acces Object is used to store and obtain data from the database.
 */
public class EmployeeDAO {
	@Inject
	ProjectDAO projectDAO;

	private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("employee_persistence");
	private EntityManager entityManager = emfactory.createEntityManager();

	/**
	 * Returns a department with a specific name given as parameter.
	 */
	public Department findDepartmentByName(String departmentName) {
		Query query = entityManager.createQuery("SELECT d FROM Department d WHERE d.departmentName LIKE :name");
		query.setParameter("name", departmentName);
		Department department = null;
		try {
			entityManager.getCriteriaBuilder();
			department = (Department) query.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return department;
	}

	/**
	 * Gets all the employees in the database. Returns a list of employees.
	 */
	public List<Employee> findAll() {
		Query query = entityManager.createQuery("SELECT e FROM Employee e");
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			entityManager.getCriteriaBuilder();
			employeeList = query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return employeeList;
	}

	/**
	 * Gets the employee with a specific id from the database. Returns the
	 * employee or null if the id is not found.
	 */
	public Employee findById(Long employeeId) {
		Employee employee = null;
		try {
			employee = entityManager.find(Employee.class, employeeId);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return employee;
	}

	/**
	 * Gets the employees with a specific name from the database. Returns the
	 * list of employees or an empty list if the name is not found.
	 */
	public List<Employee> findByName(String employeeName) {

		Query query = entityManager.createQuery("SELECT e FROM Employee e WHERE e.name LIKE :name");
		query.setParameter("name", employeeName);
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			entityManager.getCriteriaBuilder();
			employeeList = query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return employeeList;

	}

	/**
	 * Creates a new employee in the database.
	 */
	public Employee insertEmployee(Employee employee) {
		Department department = findDepartmentByName(employee.getDepartment().getDepartmentName());
		Set<Project> employeeProjects = employee.getProjects();
		Set<Project> projectsList = new HashSet<Project>();
		try {
			entityManager.getTransaction().begin();
			if (employeeProjects != null) {
				for (Project project : employeeProjects) {
					Project projectToAdd = projectDAO.findProjectByName(project.getProjectName());
					if (projectToAdd != null)
						projectsList.add(projectToAdd);
					else
						projectsList.add(project);
				}
				employee.setProjects(projectsList);
			}
			if (department == null)
				entityManager.persist(employee);
			else {
				employee.setDepartment(department);
				entityManager.persist(employee);
			}
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return employee;
	}

	/**
	 * Allows changing the informations about an employee in the database.
	 */
	public Employee updateEmployee(Employee employee) {
		Department department = findDepartmentByName(employee.getDepartment().getDepartmentName());
		Set<Project> employeeProjects = employee.getProjects();
		Set<Project> projectsList = new HashSet<Project>();
		try {
			entityManager.getTransaction().begin();
			if (employeeProjects != null) {
				for (Project project : employeeProjects) {
					Project projectToAdd = projectDAO.findProjectByName(project.getProjectName());
					if (projectToAdd != null)
						projectsList.add(projectToAdd);
					else
						projectsList.add(project);
				}
			}
			employee.setProjects(projectsList);
			if (department == null)
				entityManager.merge(employee);
			else {
				employee.setDepartment(department);
				entityManager.merge(employee);
			}
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return employee;
	}

	/**
	 * Deletes an employee with a specific id in the database.
	 */
	public Long deleteEmployee(Long employeeId) {
		Employee emp = null;
		try {
			entityManager.getTransaction().begin();
			emp = entityManager.find(Employee.class, employeeId);
			emp.setDepartment(null);
			emp.setProjects(null);
			entityManager.remove(emp);
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return employeeId;
	}

}
