package pentastagiu.employee_manager.conf;

import org.glassfish.jersey.server.ResourceConfig;

public class EmployeeWSConf extends ResourceConfig {
	public EmployeeWSConf() {
		register(new EmployeeWSBinder());
		packages(true, "pentastagiu.employee_manager.ws");
	}

}
