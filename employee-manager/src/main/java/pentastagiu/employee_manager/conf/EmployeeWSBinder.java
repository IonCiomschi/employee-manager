package pentastagiu.employee_manager.conf;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import pentastagiu.employee_manager.dao.EmployeeDAO;
import pentastagiu.employee_manager.dao.ProjectDAO;
import pentastagiu.employee_manager.service.EmployeeService;
import pentastagiu.employee_manager.service.ProjectService;
import pentastagiu.employee_manager.service.impl.EmployeeServiceImpl;
import pentastagiu.employee_manager.service.impl.ProjectServiceImpl;

public class EmployeeWSBinder extends AbstractBinder {

	@Override
	protected void configure() {
		bind(EmployeeServiceImpl.class).to(EmployeeService.class);
		bind(EmployeeDAO.class).to(EmployeeDAO.class);
		bind(ProjectServiceImpl.class).to(ProjectService.class);
		bind(ProjectDAO.class).to(ProjectDAO.class);
	}

}
