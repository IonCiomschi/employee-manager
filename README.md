# Employee Manager #

### About this project ###

* Employee Manager is an application meant to help gestionate the employees in a company capabile to show or update the informations about them. 


### Configuration details ###

* In order to run the application you need to install MySQL. 
* Create a new schema named "employeedb".
* In employee-manager/src/main/resources/META-INF/persistence.xml change the values for username and password according to your database.


### Reference Manual ###

* The application allows you to perform the following requests:

#### Add employee #####
Method: POST    
URL: <host>[:port]/employee-manager/rest/employee     
Input type: JSON           
Example of input:   
{     
    "name":"Ioana",     
    "department":{"departmentName":"HR"},     
    "social_id":{"documentIdentifier":"123456","documentType":"Passport"},      
    "projects":[{"projectName":"P1"},{"projectName":"P2"},{"projectName":"P3"}]     
}       

#### Find employee by name ####
Method: GET    
URL: <host>[:port]/employee-manager/rest/employee/name/<employee.name>    
Output type: JSON   
 
#### Find employee by id ####
Method: GET    
URL: <host>[:port]/employee-manager/rest/employee/<employee.id>    
Output type: JSON    

#### Get all employees ####
Method: GET    
URL: <host>[:port]/employee-manager/rest/employee      
Output type: JSON    

#### Update employee ####
Method: PUT    
URL: <host>[:port]/employee-manager/rest/employee      
Output type: JSON    
Example of input:   
{     
    "name":"Florin",     
    "department":{"departmentName":"ITC"},     
    "social_id":{"documentIdentifier":"122456","documentType":"Passport"}       
}   

#### Delete employee ####
Method: DELETE      
URL: <host>[:port]/employee-manager/rest/employee/<employee.id>